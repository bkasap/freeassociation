![](01.png =20x)

## Freeass-bot

A [word2vec model](https://github.com/kavgan/nlp-in-practice) trained with [sci-fi corpus](https://www.kaggle.com/jannesklaas/scifi-stories-text-corpus).

Served with [python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot).

1. Contact __BotFather__ to register bot.

2. Export bot token as environment variable.
```export TOKEN=YOURTOKENFROMBOTFATHER```

### Run locally in conda environment
3. Create conda environment
```conda env create -n freeass -f environment.yml```

4. Activate environment
```conda activate freeass```

5. Run bot backend
```python freeass.py```

### Pull docker image and run
3. Pull docker image
```docker pull bkasap/freeassociation:latest```

4. Run the image
```docker run -d -e TOKEN bkasap/freeassociation```