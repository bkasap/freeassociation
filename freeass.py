# -*- coding: utf-8 -*-
"""Simple Bot to reply to Telegram messages.
This is built on the API wrapper, see echobot2.py to see the same example built
on the telegram.ext bot framework.
This program is dedicated to the public domain under the CC0 license.
"""
import logging
import telegram
from telegram.error import NetworkError, Unauthorized
from time import sleep
import os

update_id = None

import gensim
import pickle
import random

with open('w2vscifi.pkl', 'rb') as f:
    model = pickle.load(f)

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

def pos_topshot(query):
    w = query.split()
    try:
        words = model.wv.most_similar(positive=w,topn=1)
        respond = words[0][0]
    except KeyError as e:
        respond = random.choice( 
            ["I don't know "+query+". I only see text." ,
             query+" is not in my dictionary"])
    return respond

def pos_randomshot(query):
    w = query.split()
    try:
        words = model.wv.most_similar(positive=w,topn=12)
        choice = random.choices(words, k=2)
        respond = choice[0][0]+' '+choice[1][0]
    except KeyError as e:
        respond = ''
    return respond

def neg_randomshot(query):
    w = query.split()
    try:
        words = model.wv.most_similar(negative=w,topn=12)
        choice = random.choices(words, k=2)
        respond = choice[0][0]+' '+choice[1][0]
    except KeyError as e:
        respond = ''
    return respond

def main():
    """Run the bot."""
    global update_id
    # Telegram Bot Authorization Token
    bot = telegram.Bot(os.environ["TOKEN"])

    # get the first pending update_id, this is so we can skip over it in case
    # we get an "Unauthorized" exception.
    try:
        update_id = bot.get_updates()[0].update_id
    except IndexError:
        update_id = None

    while True:
        try:
            echo(bot)
        except NetworkError:
            sleep(1)
        except Unauthorized:
            # The user has removed or blocked the bot.
            update_id += 1

def echo(bot):
    """Echo the message the user sent."""
    global update_id

    # Request updates after the last update_id
    for update in bot.get_updates(offset=update_id, timeout=10):
        update_id = update.update_id + 1

        if update.message:  # your bot can receive updates without messages
            # Reply to the message
            logger.info("USER %s: message %s", update.message.from_user.id, update.message.text)
            try:
                response = pos_topshot(update.message.text)+' ' \
                    +pos_randomshot(update.message.text)
            except (AttributeError, TypeError):
                response = 'I just can see text :dissappointed_relieved:.'
            update.message.reply_text(response)

if __name__ == '__main__':
    main()